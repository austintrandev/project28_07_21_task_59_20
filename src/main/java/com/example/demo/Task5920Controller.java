package com.example.demo;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Task5920Controller {

	@CrossOrigin
	@GetMapping("/listAnimal")
	public ArrayList<Animals> getAnimals() {
		ArrayList<Animals> listAnimals = new ArrayList<Animals>();
		Duck myDuck = new Duck();
		Fish myFish = new Fish(5, "female", 6, true);
		Zebra myZebra = new Zebra();
		listAnimals.add(myDuck);
		listAnimals.add(myFish);
		listAnimals.add(myZebra);
		return listAnimals;
	}
}
